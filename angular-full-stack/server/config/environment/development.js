'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  // mongo: {
  //   uri: 'mongodb://tester:ovnbkom15@c6.cockney.4.mongolayer.com:10006,cockney.5.mongolayer.com:10006,cockney.4.mongolayer.com:10006/kwdrocks?replicaSet=set-56422798564f1bcc1e0004fc'
  // },  

  mongo: {
    uri: 'mongodb://localhost/angularfullstack-dev'
  },

  seedDB: false
};
