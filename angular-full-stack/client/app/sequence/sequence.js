'use strict';

angular.module('angularFullStackApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('sequence', {
        url: '/Sequence',
        templateUrl: 'app/sequence/sequence.html',
        controller: 'SequenceCtrl'
      });
  });