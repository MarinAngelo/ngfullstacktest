'use strict';

describe('Controller: SequenceCtrl', function () {

  // load the controller's module
  beforeEach(module('angularFullStackApp'));

  var SequenceCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SequenceCtrl = $controller('SequenceCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
